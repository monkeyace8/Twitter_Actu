package com.actu.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.actu.model.TweetModel;

public class TweetMapper implements RowMapper<TweetModel> {

	@Override
	public TweetModel mapRow(final ResultSet rs, final int rowNum) throws SQLException {
		
		TweetModel tw = new TweetModel();
		tw.setUser_name(rs.getString("user_name"));
		tw.setTweet(rs.getString("tweet"));

		return tw;

	}

}
