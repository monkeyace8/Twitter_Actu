package com.actu.services;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import com.actu.bot.NoirNinjaBot;
import com.actu.utils.Twitter4JUtils;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;

@Service
public class TweeterService implements ITweeterService  {

	public static final Logger LOGGER = Logger.getLogger(TweeterService.class.getName());

	// Twitter
	Twitter4JUtils tUtils = new Twitter4JUtils();
	ConfigurationBuilder cb = tUtils.getTwitterInstance();
	TwitterFactory tf = new TwitterFactory(cb.build());
	Twitter tInstance = tf.getInstance();
	
	// Telegram
	Update updateTest = new Update();
	Message msg = new Message();
	List<Status> retur_list = new ArrayList<Status>();
    
	public void getStreamTL(NoirNinjaBot nn_actu) throws IllegalStateException, TwitterException {
		Twitter4JUtils tUtils = new Twitter4JUtils();
		ConfigurationBuilder cb = tUtils.getTwitterInstance();
		TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
		twitterStream.addListener(new StatusListener() {

			@Override
			public void onException(Exception ex) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onScrubGeo(long userId, long upToStatusId) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStallWarning(StallWarning warning) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onStatus(Status status) {				
				msg.setText(FormatTweet(status.getText())); 
				updateTest.setMessage(msg);
				nn_actu.onUpdateReceived(updateTest);			 				
			}
		});

		Long tw_user_id = twitterStream.getId();
		FilterQuery tweetFilterQuery = new FilterQuery();
		tweetFilterQuery.follow(new long[] { tw_user_id });
		tweetFilterQuery.language(new String[] { "fr" });
		twitterStream.filter(tweetFilterQuery);

	}


	private String FormatTweet(String txt_tw) {
		StringBuilder sb = new StringBuilder();
		sb.append("*Nouveau Tweet : \"*" + txt_tw);
		sb.append("\n");
		return sb.toString();
	}
}
