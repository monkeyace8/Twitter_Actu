package com.actu.services;

import com.actu.bot.NoirNinjaBot;

import twitter4j.TwitterException;

public interface ITweeterService {

	public static final TweeterService ts = new TweeterService();

	static void getStreamTL(NoirNinjaBot nn_actu) throws IllegalStateException, TwitterException {
		ts.getStreamTL(nn_actu);

	}
}
