package com.actu;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import com.actu.bot.NoirNinjaBot;
import com.actu.services.ITweeterService;
import com.actu.utils.Twitter4JUtils;

import springfox.documentation.swagger2.annotations.EnableSwagger2;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

@SpringBootApplication
@ComponentScan({ "com" })
@EnableSwagger2
public class NoirNinjaActuApplication {

	public static final Logger LOGGER = Logger.getLogger(NoirNinjaActuApplication.class.getName());

	private static NoirNinjaBot noirninjaStatic;

	static Twitter4JUtils tUtils = new Twitter4JUtils();
	static ConfigurationBuilder cb = tUtils.getTwitterInstance();
	static TwitterFactory tf = new TwitterFactory(cb.build());
	static Twitter tInstance = tf.getInstance();

	static Update updateTest = new Update();
	static Message msg = new Message();
	static String get_message;
	static List<Status> retur_list = new ArrayList<Status>();

	public static void main(String[] args) throws TelegramApiException, TwitterException {
		SpringApplication.run(NoirNinjaActuApplication.class, args);
		try {
			final TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);
			botsApi.registerBot(noirninjaStatic);
			LOGGER.info("NoirNinjaBot est démarré...");
			ITweeterService.getStreamTL(noirninjaStatic);
		} catch (final TelegramApiException e) {
			LOGGER.log(Level.SEVERE, "MSG : " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Autowired
	public void setNoirNinjaBot(final NoirNinjaBot noirninjaBot) {
		NoirNinjaActuApplication.noirninjaStatic = noirninjaBot;
	}

}
