package com.actu.bot;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Value;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class NoirNinjaBot extends TelegramLongPollingBot {

	public static final Logger LOGGER = Logger.getLogger(NoirNinjaBot.class.getName());
    
	//Variables d'environnement
	//Elles sont configurées dans Run as -> Run configuration -> Environnement
	@Value("${telegram.bot.token}")
	private String botToken;

	@Value("${telegram.bot.name}")
	private String botName;

	@Value("${telegram.group.id}")
	private String id_noirActu;
	
	@Override
	public void onUpdateReceived(Update update) {
		// Set variables
		SendMessage message = new SendMessage();
		String message_text = update.getMessage().getText();
		message.setChatId(id_noirActu);
		message.setText(message_text);
		try {
			execute(message); 
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getBotUsername() {
		return botName;
	}

	@Override
	public String getBotToken() {
		return botToken;
	}
}
