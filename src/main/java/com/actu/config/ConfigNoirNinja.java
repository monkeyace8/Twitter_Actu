package com.actu.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.actu.bot.NoirNinjaBot;

@Configuration
@PropertySource(value = "application.properties")
public class ConfigNoirNinja {

	@Bean
	public NoirNinjaBot getNoirNinjaBot() {
		return new NoirNinjaBot();
	}
}